#include "framebuffer.h"
#include "framebufferlayer.h"

FrameBuffer::FrameBuffer()
{
    window = nullptr;
    textureID = 0;
    _width = 1;
    _height = 1;
    _initialized = false;
    _packedDataPointer = nullptr;
}


FrameBuffer::~FrameBuffer()
{

}


void FrameBuffer::initialize(int width, int height)
{
    setSize(width, height);
    createTexture();
}


int FrameBuffer::size()
{
    if (_width < 0 || _height < 0)
        return 0;

    return _width * _height;
}


void FrameBuffer::setSize(int width, int height)
{
    _width = _height = 0;
    if (width && height)
    {
        _width = width;
        _height = height;
    }
}


void FrameBuffer::setContext(GLFWwindow *context)
{
    window = context;
}

void FrameBuffer::createTexture()
{
    glfwMakeContextCurrent(window);

    // Create one OpenGL texture
    glGenTextures(1, &textureID);

    // "Bind" the newly created texture : all future texture functions will modify this texture
    glBindTexture(GL_TEXTURE_2D, textureID);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glBindTexture(GL_TEXTURE_2D, 0);

    glfwMakeContextCurrent(0);
}


void FrameBuffer::copyDataToGPU()
{
    glfwMakeContextCurrent(window);

    float* data = nullptr;
    data = createPackedData(4, 4);
    glBindTexture(GL_TEXTURE_2D, textureID);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, width(), height(), 0, GL_RGBA, GL_FLOAT, data);
    glBindTexture(GL_TEXTURE_2D, 0);
    delete data;

    glfwMakeContextCurrent(0);
}


float* FrameBuffer::createPackedData(int channels, int stride)
{
    float* data = new float[size() * channels];

    for (int i = 0; i < channels; ++i)
    {
        if (i < _layers.size())
        {
            FrameBufferLayer* fbl = _layers.at(i);
            if (fbl && fbl->initialized() && fbl->allocatedSize())
            {
                for (int c = 0; c < fbl->size(); ++c)
                {
                    data[c * stride + i] = fbl->data()[c];
                }
            }
        } else {
            for (int c = 0; c < size(); ++c)
            {
                data[c * stride + i] = 0.0;
            }
        }
    }
    return data;
}


float* FrameBuffer::packedData()
{
    return _packedDataPointer;
}


float* FrameBuffer::packedDataPointer(int size, int channels)
{
    if (_packedDataPointer)
        delete _packedDataPointer;

    _packedDataPointer = new float[size * channels];
    return _packedDataPointer;
}


void FrameBuffer::setLayerCount(int numLayers)
{
    deleteAll();
    _layers.resize(numLayers);
    for (int i = 0; i < _layers.size(); ++i)
    {
        _layers.at(i) = nullptr;
    }
}


void FrameBuffer::addLayer(FrameBufferLayer* layer, int position)
{
    // If layer itself is not null or garbage, add it to position
    if (layer)
    {
        if (_layers.size() > position)
            _layers.at(position) = layer;
    }
}


void FrameBuffer::deleteAll()
{
    for (int i = 0; i < _layers.size(); ++i)
    {
        FrameBufferLayer* fbl = _layers.at(i);
        fbl->deinitialize();
        //delete fbl;
    }
    _layers.clear();
}
