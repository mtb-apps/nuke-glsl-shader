// GLSL shader node for Nuke

// Use the Windows 7 SDK MSVC 2010 x64 compiler for Nuke 10.5
// Visual Studio 2015 Update 3 - (Manifest Version: 14.0.24210) for Nuke 11

// For linux, gcc version 4.8.x should work, current Nuke 12 and 12.1 versions
// are built on Centos 7.6 using gcc 4.8.5

// Link against proper Nuke dot version, for example for building plugin for Nuke 11.2,
// it is necessary to link against Nuke 11.2 libs! Other dot versions DO NOT WORK, plugin will not load.


#define MAIN
#define _NO_CRT_STDIO_INLINE

static const char* const CLASS = "NukeGLSL";
static const char* const HELP = "Nuke GLSL shader execution node";

// Nuke SDK includes
#include "DDImage/Op.h"
#include "DDImage/Iop.h"
#include "DDImage/DrawIop.h"
#include "DDImage/PixelIop.h"
#include "DDImage/Black.h"
#include "DDImage/CameraOp.h"
#include "DDImage/GeoOp.h"

#include "DDImage/Knobs.h"
#include "DDImage/DDMath.h"

#include "DDImage/Knob.h"
#include "DDImage/Format.h"
#include "DDImage/Channel3D.h"
#include "DDImage/Interest.h"
#include "DDImage/Row.h"
#include "DDImage/Tile.h"
#include "DDImage/ChannelSet.h"
#include "DDImage/Hash.h"
#include "DDImage/NukeWrapper.h"

#include "DDImage/DDImage_API.h"

// General includes
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <assert.h>
#include <fstream>
#include <ctime>
#include <algorithm>
#include <streambuf>

// OpenGL related includes
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#ifdef _WIN32
#pragma comment(lib, "opengl32")
#endif

// NukeGLSL internal classes
#include "framebuffer.h"
#include "framebufferlayer.h"

// Cam input is currently hidden behind texture inputs, I couldn't figure out how to show it
// but still have it optional (so that additional image inputs would show without it connected)
#define MAX_INPUTS 5
#define INPUT_TEXTURE0 0
#define INPUT_TEXTURE1 1
#define INPUT_TEXTURE2 2
#define INPUT_TEXTURE3 3
#define INPUT_CAM 4


// Struct that is used for moving around camera input data
struct CamData
{
    float filmback[2];
    float nearDistance;
    float farDistance;
    float focal_point;
    float fstop;
    float win_translate[2];
    float win_scale[2];
    float win_roll;
    float focal_length;
    float position[3];
    float matrix[16];
    float imatrix[16];
    float projection[16];
    float proj_mode;
    float projection_is_linear;
};


// Helper function that is used for glfw error callback
static void error_callback(int error, const char *description)
{
    fprintf( stderr, description );
    fprintf( stderr, "\n" );
}


// Generic helper that converts numeric values to string representation for logging
template <typename T> std::string tostr(const T& t) {
   std::ostringstream os;
   os << t;
   return os.str();
}



class NukeGLSL : public DD::Image::Iop
{
private:
    // Knob stuff
    DD::Image::FormatPair formats;
    bool use_input0_format;
    bool use_camera;
    const char* filename;
    bool usetime;
    bool debug;
    bool autocleanup;
    const char* vertexShaderCode;
    const char* fragmentShaderCode;
    const char* cleanVertexShaderCode;
    const char* cleanFragmentShaderCode;
    std::string originalCode;
    std::string cleanCode;

    float frameRate;
    float mouseposition[2];
    int numIterations;
    CamData cameraData;

    // Input and output framebuffer related stuff
    int newWidth, newHeight;
    std::vector<FrameBuffer*> inputFrameBuffers;
    FrameBuffer* outputFrameBuffer;
    FrameBuffer* intermediateFrameBuffer;
    GLuint RenderFramebuffer;
    GLuint FinalFramebuffer;
    GLuint renderFramebufferTexture;
    GLuint finalFramebufferTexture;
    bool oglBuffersCreated;

    // OpenGL related data
    GLFWwindow* window;
    GLuint programID;
    GLuint vertexArrayID;
    GLuint vertexBuffer;

    // General housekeeping
    bool openCalled;
    bool windowCreated;
    bool shaderCompiled;


public:
    int maximum_inputs() const { return MAX_INPUTS; }
    int minimum_inputs() const { return 1; }

    void _validate(bool);
    void _request(int x, int y, int r, int t, DD::Image::ChannelMask channels, int count);
    void _open();
    void engine ( int y, int x, int r, DD::Image::ChannelMask channels, DD::Image::Row& out );

    void append(DD::Image::Hash& hash);

    // make sure that all members are initialized
    NukeGLSL(Node* node) : Iop(node)
    {
        use_input0_format = false;
        use_camera = false;
        filename = "";
        usetime = true;
        debug = false;
        autocleanup = true;
        frameRate = 24.0f;
        mouseposition[0] = mouseposition[1] = 0.0f;
        numIterations = 1;
        newWidth = 1280;
        newHeight = 720;

        vertexShaderCode = "void main(){\n"
                           "    vec3 pos = gl_Vertex.xyz;"
                           "    gl_Position = vec4(pos, 1.0);\n"
                           "};\n";

        fragmentShaderCode = "#version 120\n"
                             "uniform float iTime;\n"
                             "uniform vec2 iResolution;\n"
                             "void main(){\n"
                             "    gl_FragColor = vec4(1, 0, 0, 1);\n"
                             "};\n";

        cleanVertexShaderCode = "void main(){\n"
                                "    vec3 pos = gl_Vertex.xyz;"
                                "    gl_Position = vec4(pos, 1.0);\n"
                                "};\n";

        cleanFragmentShaderCode = "#version 120\n"
                                  "uniform float iTime;\n"
                                  "uniform vec2 iResolution;\n"
                                  "void main(){\n"
                                  "    gl_FragColor = vec4(1, 0, 0, 1);\n"
                                  "};\n";

        outputFrameBuffer = nullptr;
        intermediateFrameBuffer = nullptr;
        RenderFramebuffer = 0;
        FinalFramebuffer = 0;
        oglBuffersCreated = false;
        window = NULL;
        programID = 0;
        vertexArrayID = 0;
        vertexBuffer = 0;

        openCalled = false;
        windowCreated = false;
        shaderCompiled = false;
    }

    virtual void knobs(DD::Image::Knob_Callback);
    int knob_changed(DD::Image::Knob* k);
    const char* input_label(int input, char* buffer) const;
    DD::Image::Op* default_input(int input) const;
    bool test_input(int input, DD::Image::Op* op) const;

    const char* Class() const { return CLASS; }
    const char* node_help() const { return HELP; }
    const char* displayName() const { return "NukeGLSL"; }
    static const Iop::Description description;

    std::string readShaderFile(std::string filename);
    void readFragmentShaderFile();
    void logMessage(std::string logMessage);
    bool inputUsedInShader(int input);
    void clearFrameBuffers();
    void updateFrameBuffers();
    bool createOGLBuffers();
    bool createWindow();
    void changeResolution();
    void cleanUpFragmentShaderCode(const char* shadercode);
    void addLineNumbersAndPrint(const char* inputText);
    GLuint loadShadersFromCode(const char* vertex_shader_code, const char* fragment_shader_code);
    bool compileShader();
    bool drawToWindow(int currentIteration);
    void terminateGL();
};



void NukeGLSL::knobs(DD::Image::Knob_Callback f)
{
    DD::Image::Format_knob(f, &formats, "format");
    DD::Image::Bool_knob(f, &use_input0_format, "use_input0_format", "Use format of input 0");
    DD::Image::Bool_knob(f, &debug, "debug", "Debug logging");
    DD::Image::SetFlags(f, DD::Image::Knob::STARTLINE);
    DD::Image::Bool_knob(f, &autocleanup, "autocleanup", "Auto-clean shader code");
    DD::Image::File_knob(f, &filename, "shader_file", "Shader file");
    DD::Image::SetFlags(f, DD::Image::Knob::STARTLINE);
    Divider(f);
    DD::Image::Multiline_String_knob(f, &fragmentShaderCode, "fragment_shader_code", "Fragment Shader", 100);
    DD::Image::SetFlags(f, DD::Image::Knob::NO_ANIMATION);  // NO_ANIMATION flag is essential to prevent triggering expression evaluation in knob
    DD::Image::Button(f, "recompile_shader", "Recompile Shader");
    DD::Image::SetFlags(f, DD::Image::Knob::STARTLINE);
    Divider(f);
    DD::Image::Float_knob(f, &frameRate, "framerate", "Frame rate");
    DD::Image::XY_knob(f, mouseposition, "mouseposition", "Mouse position");
    DD::Image::Int_knob(f, &numIterations, "numiterations", "Number of iterations");
}


// Most knob changes violate compiled shader, so we mark compilation flag as false
// It isn't very nice currently

int NukeGLSL::knob_changed(DD::Image::Knob* k)
{
    if(k->is("shader_file"))
    {
        readFragmentShaderFile();
        knob("fragment_shader_code")->set_text(originalCode.data());
        compileShader();
        return true;
    }

    if(k->is("fragment_shader_code"))
    {
        compileShader();
        return true;
    }

    if(k->is("autocleanup"))
    {
        compileShader();
        return true;
    }

    if(k->is("recompile_shader"))
    {
        compileShader();
        return true;
    }

    if(k->is("formats"))
    {
        compileShader();
        return true;
    }

    if(k->is("use_input0_format"))
    {
        compileShader();
        return true;
    }

    return DD::Image::Iop::knob_changed(k);
}


const char* NukeGLSL::input_label(int input, char* buffer) const
{
    switch (input)
    {
        case INPUT_TEXTURE0:
            return "iTexture0";
        case INPUT_TEXTURE1:
            return "iTexture1";
        case INPUT_TEXTURE2:
            return "iTexture2";
        case INPUT_TEXTURE3:
            return "iTexture3";
        case INPUT_CAM:
            return "cam";
        default:
            return "iTexture";
    }
}


DD::Image::Op* NukeGLSL::default_input(int input) const
{
    if (input == INPUT_CAM)
        return 0;
    return Iop::default_input(input);
}


// Input testing makes sure only correct inputs can be attached to node inputs
// For camera, we cast input to camera class and if it succeeds, we allow connection
bool NukeGLSL::test_input(int input, DD::Image::Op* op) const
{
    if (input == INPUT_CAM)
    {
        return dynamic_cast<DD::Image::CameraOp*>(op) != 0;
    }
    return DD::Image::Iop::test_input(input, op);
}


// Generic read file from disk helper
std::string NukeGLSL::readShaderFile(std::string filename)
{
    std::ifstream file(filename);

    if (!file.is_open())
        return "";

    std::string str((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());
    return str;
}


// Read fragment shader file from disk
void NukeGLSL::readFragmentShaderFile()
{
    originalCode = readShaderFile(filename);
}


// Generic helper logger
void NukeGLSL::logMessage(std::string message)
{
    if (debug)
    {
        std::cout << message << std::endl;
    }
}


// Validate calls validate on first input and if so set,
// updates the node format and recreates OpenGL context.
// Nothing else happens here to keep it fast

void NukeGLSL::_validate(bool for_real)
{
    // Validate all inputs
    for (int i = 0; i < MAX_INPUTS; ++i)
    {
        if (input(i))
        {
            input(i)->validate(for_real);
        }
    }


    // Create format based on format combobox.
    // If we have use first input checkbox checked, derive format from that.
    // If input is disconnected, don't use it because disconnected input format is 1 pixel
    bool input0Connected = true;
    if (dynamic_cast<DD::Image::Black*>(input(0)))
        input0Connected = false;

    if (use_input0_format && input0Connected)
    {
        copy_info();
    }
    else
    {
        info_.full_size_format(*formats.fullSizeFormat());
        info_.format(*formats.format());
        info_.channels(DD::Image::Mask_RGBA);
        info_.set(format());

        DD::Image::Format curFormat = *formats.fullSizeFormat();
        newWidth = curFormat.width();
        newHeight = curFormat.height();
    }

    // If we don't have framebuffer yet, create it
    if (!outputFrameBuffer)
    {
        outputFrameBuffer = new FrameBuffer();
        intermediateFrameBuffer = new FrameBuffer();
        oglBuffersCreated = false;
    }


    // If size has changed, change buffer sizes and recreate window and context
    if (input(INPUT_TEXTURE0))
    {
        // If we want to use input format, use size from that if connected
        if (use_input0_format && input0Connected)
        {
            newWidth = input(INPUT_TEXTURE0)->format().width();
            newHeight = input(INPUT_TEXTURE0)->format().height();
        }

        if (newWidth != outputFrameBuffer->width() || newHeight != outputFrameBuffer->height())
        {
            // outputFrameBuffer->setSize(newWidth, newHeight);
            // outputFrameBuffer->setContext(window);
            // outputFrameBuffer->createTexture();
            // outputFrameBuffer->setInitialized(true);
            //
            // intermediateFrameBuffer->setSize(newWidth, newHeight);
            // intermediateFrameBuffer->setContext(window);
            // intermediateFrameBuffer->createTexture();
            // intermediateFrameBuffer->setInitialized(true);

            oglBuffersCreated = false;
            changeResolution();
            //updateFrameBuffers();
            compileShader();
            //createOGLBuffers(); // test if this makes any difference in multiple node instance situation
        }
    }

    openCalled = false;
}


void NukeGLSL::_request(int x, int y, int r, int t, DD::Image::ChannelMask channels, int count)
{
    for (int i = 0; i < MAX_INPUTS; ++i)
    {
        if (input(i) && i != INPUT_CAM)
        {
          const DD::Image::Box& b = input(i)->info();
          input(i)->request(b.x(), b.y(), b.r(), b.t(), DD::Image::Mask_RGBA, count);
        }
    }
}


// Here we read data from input textures and copy data to OpenGL texture buffers
void NukeGLSL::_open()
{
//    if (openCalled)
//        return;

    // Clear all data first
    clearFrameBuffers();
    inputFrameBuffers.resize(MAX_INPUTS);

    if (!window)
    {
        logMessage("Window context has gone missing");
    }

    // It appears that window context will drop when switching between multiple nodes
    // this tries to recreate context in these cases
    glfwMakeContextCurrent(window);
    if (!glfwGetCurrentContext())
    {
        glfwMakeContextCurrent(NULL);
        oglBuffersCreated = false;
        changeResolution();
        createOGLBuffers();

        shaderCompiled = false;
        compileShader();

        glfwMakeContextCurrent(NULL);
    }

    // Start timer to time memory operations
    clock_t begin_time = clock();

    // Iterate over inputs
    for (int i = 0; i < MAX_INPUTS; ++i)
    {
        // New framebuffer object that will hold input texture data
        // It is created before checks to have a vector with size of inputs
        logMessage("Input number: " + tostr(i));

        if (i == INPUT_CAM)
        {

            DD::Image::CameraOp* camInput = dynamic_cast<DD::Image::CameraOp*>(DD::Image::Op::input(i));
            if (camInput)
            {
                cameraData.focal_length = camInput->focal_length();
                memcpy(cameraData.projection, camInput->projection(camInput->projection_mode()).array(), 16*sizeof(float));
                memcpy(cameraData.matrix, camInput->matrix().array(), 16*sizeof(float));
                memcpy(cameraData.imatrix, camInput->imatrix().array(), 16*sizeof(float));
                cameraData.position[0] = camInput->imatrix().a03;
                cameraData.position[1] = camInput->imatrix().a13;
                cameraData.position[2] = camInput->imatrix().a23;
                cameraData.filmback[0] = camInput->film_width();
                cameraData.filmback[1] = camInput->film_height();
                cameraData.nearDistance = camInput->Near();
                cameraData.farDistance = camInput->Far();
                cameraData.focal_point = camInput->focal_point();
                cameraData.fstop = camInput->fstop();
                cameraData.win_translate[0] = camInput->win_translate().x;
                cameraData.win_translate[1] = camInput->win_translate().y;
                cameraData.win_scale[0] = camInput->win_scale().x;
                cameraData.win_scale[1] = camInput->win_scale().y;
                cameraData.win_roll = camInput->win_roll();
                cameraData.proj_mode = camInput->projection_mode();
                cameraData.projection_is_linear = camInput->projection_is_linear(camInput->projection_mode());
            }
            continue;
        }

        // Check that input is connected and that input is used in shader at all
        Iop* curInput = input(i);
        if (!curInput || !inputUsedInShader(i))
            continue;

        // Only create actual buffer if input is connected and used
        FrameBuffer* curFrameBuffer = new FrameBuffer();
        inputFrameBuffers.at(i) = curFrameBuffer;

        int width, height;
        width = curInput->format().width();
        height = curInput->format().height();
        U64 hash = curInput->hash().value();
        logMessage("Width: " + tostr(width));
        logMessage("Height: " + tostr(height));
        logMessage("Hash: " + tostr(hash));

        bool disconnected = false;
        if (dynamic_cast<DD::Image::Black*>(input(i)))
        {
            disconnected = true;
            logMessage("Input disconnected");
        }

        // Context MUST be set before initialization
        curFrameBuffer->setContext(window);
        logMessage("Context set");
        curFrameBuffer->setSize(width, height);
        logMessage("Framebuffer size set");
        curFrameBuffer->createTexture();
        logMessage("Framebuffer texture created");
        curFrameBuffer->setName("FB_" + tostr(i));
        curFrameBuffer->setHash(hash);
        curFrameBuffer->setLayerCount(4);
        curFrameBuffer->setInitialized(true);

    	logMessage("Framebuffer initialized");

        //Channels from input and RGBA channel mask to intersect with
        DD::Image::ChannelSet chmask, inputChannels;
        chmask += DD::Image::Mask_RGBA;
        inputChannels = curInput->channels();

        // For reading we want to access the input format, not bbox.
        DD::Image::Format format = curInput->format();
        const int fx = format.x();
        const int fy = format.y();
        const int fr = format.r();
        const int ft = format.t();

        // Lock an area into the cache using an interest and release immediately
        // Why it is done this way, beats me, but example scripts do this also
        DD::Image::Interest interest(*curInput, fx, fy, fr, ft, chmask, true);
        interest.unlock();

        foreach(channel, chmask)
        {
            // New input layer set that will be filled with data
            FrameBufferLayer* curLayer = new FrameBufferLayer();
            curFrameBuffer->addLayer(curLayer, channel - 1);    // -1 is because channel 0 is black and we want index to start at 0 with red

            // Intersect channel with input channels
            if (inputChannels.contains(channel))
            {
                curLayer->setChannel(channel);
                curLayer->initialize(width, height);

                if (curLayer->allocatedSize() < width*height)
                {
                    logMessage("Allocated too little memory in framebuffer layer");
                    continue;
                }

                // Iterate over rows and copy contents to buffer
                // Check if disconnected, not necessary to copy anything if is
                if (!disconnected)
                {
                    int currentRow = 0;
                    for (int ry = fy; ry < ft; ry++)
                    {
                        DD::Image::Row row(fx, fr);
                        row.get(*curInput, ry, fx, fr, chmask);

                        const float *CUR = row[channel] + fx;
                        const int rowPtr = currentRow * width;
                        float *dst = &curLayer->data()[rowPtr];
                        memcpy(dst, CUR, sizeof(float) * width);
                        currentRow++;
                    }

                    // This debug part is just for probing values in buffers
                    if (debug)
                    {
                        int index = mouseposition[1] * curLayer->width() + mouseposition[0];
                        float value = curLayer->data()[index];
                        logMessage("Framebufferlayer value: " + tostr(value));
                    }
                }

            } // channel intersect
        } // foreach

        // Now that framebuffer is filled, copy data to gpu for use.
        // Check for disconnected because if there is no input (meaning black image) there is nothing to copy
        if (!disconnected)
            curFrameBuffer->copyDataToGPU();
    } // inputs

    if (debug)
	std::cout << "Inputs copy time: " << float( clock () - begin_time ) /  CLOCKS_PER_SEC << std::endl;

    // If we have an opengl window, draw to it
    if (windowCreated)
    {
        begin_time = clock();

        if (!shaderCompiled)
            compileShader();

        drawToWindow(numIterations);

        if (debug)
	    std::cout << "Compile-Bind-Draw time: " << float( clock () - begin_time ) /  CLOCKS_PER_SEC << std::endl;
    }

    openCalled = true;
}



void NukeGLSL::append(DD::Image::Hash& hash)
{
    // If we have a shader that is animated, append frame to hash
    if (usetime)
        hash.append(outputContext().frame());
}



// This is a helper for cleaning up all input framebuffers
// Each framebuffer is responsible for cleaning up its stuff, including memory

void NukeGLSL::clearFrameBuffers()
{
    for (int i = 0; i < inputFrameBuffers.size(); ++i)
    {
        FrameBuffer* fb = inputFrameBuffers.at(i);
        if (fb)
        {
            fb->deleteAll();
            delete fb;
        }
    }
    inputFrameBuffers.clear();
}



void NukeGLSL::changeResolution()
{
    terminateGL();
    createWindow();
    updateFrameBuffers();
}

void NukeGLSL::updateFrameBuffers()
{
    outputFrameBuffer->setSize(newWidth, newHeight);
    outputFrameBuffer->setContext(window);
    outputFrameBuffer->createTexture();
    outputFrameBuffer->setInitialized(true);

    intermediateFrameBuffer->setSize(newWidth, newHeight);
    intermediateFrameBuffer->setContext(window);
    intermediateFrameBuffer->createTexture();
    intermediateFrameBuffer->setInitialized(true);
}


void NukeGLSL::terminateGL()
{
    // Close OpenGL window and terminate GLFW
    glfwTerminate();
    windowCreated = false;
    clearFrameBuffers();
}



bool NukeGLSL::createWindow()
{
    windowCreated = false;

    const int windowWidth = outputFrameBuffer->width();
    const int windowHeight = outputFrameBuffer->height();

    if( !glfwInit() )
    {
        fprintf( stderr, "Failed to initialize GLFW\n" );
        return false;
    }

    glfwSetErrorCallback(error_callback);

    glfwWindowHint(GLFW_SAMPLES, 4);

    // Version must be 2.1, otherwise Nuke viewer will go crazy
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);

    // Invisible window, because we just need it for context
    glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);

    // This forces full float processing so output is not clipped
    glfwWindowHint(GLFW_RED_BITS, 32);
    glfwWindowHint(GLFW_GREEN_BITS, 32);
    glfwWindowHint(GLFW_BLUE_BITS, 32);
    glfwWindowHint(GLFW_ALPHA_BITS, 32);

    // Open a window and create its OpenGL context
    window = glfwCreateWindow( windowWidth, windowHeight, node_name().c_str(), NULL, NULL);
    if( window == NULL ){
        fprintf( stderr, "Failed to open GLFW window.\n" );
        glfwTerminate();
        return false;
    }

    glfwMakeContextCurrent(window); // Initialize GLEW

    if (glewInit() != GLEW_OK) {
        fprintf(stderr, "Failed to initialize GLEW\n");
        window = NULL;
        glfwMakeContextCurrent(NULL);
        glfwTerminate();
        return false;
    }


    // This is the actual screen surface we draw on. A simple quad.
    glGenVertexArrays(1, &vertexArrayID);
    glBindVertexArray(vertexArrayID);

    static const GLfloat g_vertex_buffer_data[] = {
        -1.0f, -1.0f, 0.0f,
         1.0f, -1.0f, 0.0f,
         1.0f,  1.0f, 0.0f,
        -1.0f,  1.0f, 0.0f,
    };

    glGenBuffers(1, &vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);

    glfwMakeContextCurrent(NULL);

    windowCreated = true;
    return windowCreated;
}

std::string removeComments(std::string prgm)
{
    int n = prgm.length();
    std::string res;

    // Flags to indicate that single line and multpile line comments
    // have started or not.
    bool s_cmt = false;
    bool m_cmt = false;

    // Traverse the given program
    for (int i=0; i<n; i++)
    {

        // If single line comment flag is on, then check for end of it
        if (s_cmt == true && prgm[i] == '\n')
        {
            s_cmt = false;
        }

        // If multiple line comment is on, then check for end of it
        else if  (m_cmt == true && prgm[i] == '*' && prgm[i+1] == '/')
        {
            m_cmt = false,  i++;
        }

        // If this character is in a comment, ignore it
        else if (s_cmt || m_cmt)
        {
            continue;
        }

        // Check for beginning of comments and set the approproate flags
        else if (prgm[i] == '/' && prgm[i+1] == '/')
        {
            s_cmt = true, i++;
        }
        else if (prgm[i] == '/' && prgm[i+1] == '*')
        {
            m_cmt = true,  i++;
        }

        // If current character is a non-comment character, append it to res
        else  res += prgm[i];
    }

    return res;
}

void NukeGLSL::cleanUpFragmentShaderCode(const char* shadercode)
{
    originalCode = shadercode;
    if (!autocleanup)
    {
        cleanFragmentShaderCode = originalCode.data();
        return;
    }

    cleanCode = removeComments(originalCode);

    std::string voidmain("void main(");

    std::size_t found = cleanCode.find(voidmain);
    if (found == std::string::npos)
    {
        std::cout << "Main function not found, appended wrapper for mainImage." << '\n';

        std::string mainWrapper = "void main(void) { mainImage(gl_FragColor, gl_FragCoord.xy) ; }";
        cleanCode.append(mainWrapper);
    }

    // Remove first line, which may be version line, and store it
    std::string first_line(cleanCode.begin(), std::find(cleanCode.begin(), cleanCode.end(), '\n'));
    std::string versionstr("#version");
    found = first_line.find(versionstr);
    if (found != std::string::npos)
        cleanCode.erase(0, cleanCode.find("\n") + 1);


    // Prepare uniform descriptions for injection
    // This is pretty daft and should be unified with uniforms used in drawing

    std::vector<std::string> uniforms;

    // Time uniforms
    uniforms.push_back("uniform float time;\n");
    uniforms.push_back("uniform float Time;\n");
    uniforms.push_back("uniform float iTime;\n");

    uniforms.push_back("uniform float frame;\n");
    uniforms.push_back("uniform float Frame;\n");
    uniforms.push_back("uniform float iFrame;\n");

    // Check if shader code uses time uniforms
    // If it doesn't, we can set usetime to false

    usetime = false;
    for (int i = 0; i < uniforms.size(); ++i)
    {
        std::size_t found = cleanCode.find(uniforms.at(i));
        if (found != std::string::npos)
            usetime = true;
    }

    // A hacky way for Shadertoy shaders which prevents locking time evaluation
    std::vector<std::string> shaderToySpecials;
    shaderToySpecials.push_back("iTime");
    shaderToySpecials.push_back("iFrame");
    for (int i = 0; i < shaderToySpecials.size(); ++i)
    {
        std::size_t found = cleanCode.find(shaderToySpecials.at(i));
        if (found != std::string::npos)
            usetime = true;
    }

    // Unique uniforms

    uniforms.push_back("uniform float camFocalLength;\n");
    uniforms.push_back("uniform vec2 camFilmBack;\n");
    uniforms.push_back("uniform float camNear;\n");
    uniforms.push_back("uniform float camFar;\n");
    uniforms.push_back("uniform float camFocalPoint;\n");
    uniforms.push_back("uniform float camFStop;\n");
    uniforms.push_back("uniform vec2 camWinTranslate;\n");
    uniforms.push_back("uniform vec2 camWinScale;\n");
    uniforms.push_back("uniform float camWinRoll;\n");
    uniforms.push_back("uniform float camProjectionMode;\n");
    uniforms.push_back("uniform float camProjectionIsLinear;\n");
    uniforms.push_back("uniform vec3 camPosition;\n");
    uniforms.push_back("uniform mat4 camMatrix;\n");
    uniforms.push_back("uniform mat4 camMatrixInv;\n");

    uniforms.push_back("uniform sampler2D feedback;\n");
    uniforms.push_back("uniform sampler2D Feedback;\n");
    uniforms.push_back("uniform sampler2D iFeedback;\n");

    uniforms.push_back("uniform float iteration;\n");
    uniforms.push_back("uniform float Iteration;\n");
    uniforms.push_back("uniform float iIteration;\n");

    uniforms.push_back("uniform float iterationcount;\n");
    uniforms.push_back("uniform float IterationCount;\n");
    uniforms.push_back("uniform float iIterationCount;\n");

    // General and shadertoy uniforms
    uniforms.push_back("uniform vec2 resolution;\n");
    uniforms.push_back("uniform vec2 Resolution;\n");
    uniforms.push_back("uniform vec2 iResolution;\n");

    uniforms.push_back("uniform vec2 mouse;\n");
    uniforms.push_back("uniform vec2 Mouse;\n");
    uniforms.push_back("uniform vec2 iMouse;\n");

    uniforms.push_back("uniform sampler2D texture;\n");
    uniforms.push_back("uniform sampler2D texture0;\n");
    uniforms.push_back("uniform sampler2D texture1;\n");
    uniforms.push_back("uniform sampler2D texture2;\n");
    uniforms.push_back("uniform sampler2D texture3;\n");

    uniforms.push_back("uniform sampler2D Texture;\n");
    uniforms.push_back("uniform sampler2D Texture0;\n");
    uniforms.push_back("uniform sampler2D Texture1;\n");
    uniforms.push_back("uniform sampler2D Texture2;\n");
    uniforms.push_back("uniform sampler2D Texture3;\n");

    uniforms.push_back("uniform sampler2D iTexture;\n");
    uniforms.push_back("uniform sampler2D iTexture0;\n");
    uniforms.push_back("uniform sampler2D iTexture1;\n");
    uniforms.push_back("uniform sampler2D iTexture2;\n");
    uniforms.push_back("uniform sampler2D iTexture3;\n");

    uniforms.push_back("uniform sampler2D iChannel;\n");
    uniforms.push_back("uniform sampler2D iChannel0;\n");
    uniforms.push_back("uniform sampler2D iChannel1;\n");
    uniforms.push_back("uniform sampler2D iChannel2;\n");
    uniforms.push_back("uniform sampler2D iChannel3;\n");

    // Flame Matchbox uniforms
    uniforms.push_back("uniform sampler2D adsk_texture_grid;\n");

    // If uniform is not in shader code, add it
    // This prevents errors from shader using some
    // Shadertoy uniform that is missing in code
    for (int i = 0; i < uniforms.size(); ++i)
    {
        std::size_t found = cleanCode.find(uniforms.at(i));
        if (found == std::string::npos)
            cleanCode.insert(0, uniforms.at(i));
    }

    // Insert first line back
    found = first_line.find(versionstr);
    if (found != std::string::npos)
    {
        first_line.append("\n");
        cleanCode.insert(0, first_line);
    }

    cleanFragmentShaderCode = cleanCode.data();
}


void NukeGLSL::addLineNumbersAndPrint(const char* inputText)
{
    if (debug)
    {
        std::vector<std::string> strings;
        std::istringstream f(inputText);
        std::string s;
        int line = 0;
        while (std::getline(f, s, '\n')) {
            strings.push_back(s);
            line++;
        }

        for (int i = 0; i < strings.size(); ++i)
        {
            std::cout << "(" << i << "): " << strings.at(i) << std::endl;
        }
    }
}

GLuint NukeGLSL::loadShadersFromCode(const char* vertex_shader_code, const char* fragment_shader_code)
{

    // Create the shaders
    GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
    GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

    GLint Result = GL_FALSE;
    int InfoLogLength;

    // Compile Vertex Shader
    char const * VertexSourcePointer = vertex_shader_code;
    glShaderSource(VertexShaderID, 1, &VertexSourcePointer , NULL);
    glCompileShader(VertexShaderID);

    // Check Vertex Shader
    glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if ( InfoLogLength > 0 ){
        std::vector<char> VertexShaderErrorMessage(InfoLogLength+1);
        glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
        //printf("%s\n", &VertexShaderErrorMessage[0]);
        std::cout << "Vertex shader errors:" << std::endl;
        std::cout << &VertexShaderErrorMessage[0] << std::endl;
    }

    // Compile Fragment Shader
    char const * FragmentSourcePointer = fragment_shader_code;
    glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer , NULL);
    glCompileShader(FragmentShaderID);

    // Check Fragment Shader
    glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if ( InfoLogLength > 0 ){
        std::vector<char> FragmentShaderErrorMessage(InfoLogLength+1);
        glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
        //printf("%s\n", &FragmentShaderErrorMessage[0]);
        std::cout << "Fragment shader errors:" << std::endl;
        std::cout << &FragmentShaderErrorMessage[0] << std::endl;
    }

    // Link the program
    GLuint ProgramID = glCreateProgram();
    glAttachShader(ProgramID, VertexShaderID);
    glAttachShader(ProgramID, FragmentShaderID);
    glLinkProgram(ProgramID);

    // Check the program
    glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
    glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if ( InfoLogLength > 0 ){
        std::vector<char> ProgramErrorMessage(InfoLogLength+1);
        glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
        //printf("%s\n", &ProgramErrorMessage[0]);
        std::cout << "Program errors:" << std::endl;
        std::cout << &ProgramErrorMessage[0] << std::endl;
    }

    glDetachShader(ProgramID, VertexShaderID);
    glDetachShader(ProgramID, FragmentShaderID);

    glDeleteShader(VertexShaderID);
    glDeleteShader(FragmentShaderID);

    return ProgramID;
}


bool NukeGLSL::compileShader()
{
    shaderCompiled = false;

    glfwMakeContextCurrent(window);

    // Create shader program
    if (programID)
        glDeleteProgram(programID);

    cleanUpFragmentShaderCode(fragmentShaderCode);
    addLineNumbersAndPrint(cleanFragmentShaderCode);

    programID = loadShadersFromCode(vertexShaderCode, cleanFragmentShaderCode);
    if (programID)
        shaderCompiled = true;

    glfwMakeContextCurrent(NULL);

    return shaderCompiled;
}



// This is a stomp for checking if some input is actually used in shader
// If it is not, no reason to copy input data to GPU
bool NukeGLSL::inputUsedInShader(int input)
{
    return true;
}


// Here we set up opengl buffers that are used as intermediate and final render buffers in drawing
bool NukeGLSL::createOGLBuffers()
{
    oglBuffersCreated = false;

    GLsizei outputWidth = outputFrameBuffer->width();
    GLsizei outputHeight = outputFrameBuffer->height();

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Create and initialize render buffer
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    glGenFramebuffers(1, &RenderFramebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, RenderFramebuffer);

    // The texture we're going to render to
    //GLuint renderFramebufferTexture;
    glGenTextures(1, &renderFramebufferTexture);

    glBindTexture(GL_TEXTURE_2D, renderFramebufferTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, outputWidth, outputHeight, 0, GL_RGBA, GL_FLOAT, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    // Set texture as our colour attachement
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, renderFramebufferTexture, 0);

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Create and initialize final output buffer (this must be moved out from draw altogether)
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    glGenFramebuffers(1, &FinalFramebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, FinalFramebuffer);

    // The texture we're going to render to
    //GLuint FinalFramebufferTexture;
    glGenTextures(1, &finalFramebufferTexture);

    glBindTexture(GL_TEXTURE_2D, finalFramebufferTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, outputWidth, outputHeight, 0, GL_RGBA, GL_FLOAT, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    // Set texture as our colour attachement
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, finalFramebufferTexture, 0);

    // OpenGL buffer creation really needs error checking...
    oglBuffersCreated = true;
    std::cout << "OGL buffers created" << std::endl;

    return true;
}



bool NukeGLSL::drawToWindow(int currentIteration)
{

    glfwMakeContextCurrent(window);

    glClearColor(0.0f, 1.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Not sure which flags here are actually needed
    glEnable(GL_TEXTURE_2D);
    glDisable(GL_CULL_FACE);
    glDepthMask(GL_FALSE);      // disable writes to Z-Buffer
    glDisable(GL_DEPTH_TEST);   // disable depth-testing

    // Use our shader
    glUseProgram(programID);

    // Uniform stuff
    // First set up lists of possible uniform names for each parameter
    // Later use these names to probe shader for uniforms
    // Different shaders can have different names, for example Shadertoy
    // uses iTime, iFrame etc

    const char* iterUniformArgs[] = {"iIteration", "Iteration", "iteration"};
    std::vector<std::string> iterUniforms(iterUniformArgs, iterUniformArgs + 3);
    const char* iterCountUniformArgs[] = {"iIterationCount", "IterationCount", "iterationcount"};
    std::vector<std::string> iterCountUniforms(iterCountUniformArgs, iterCountUniformArgs + 3);
    const char* frameUniformArgs[] = {"iFrame", "Frame", "frame"};
    std::vector<std::string> frameUniforms(frameUniformArgs, frameUniformArgs + 3);
    const char* timeUniformArgs[] = {"iTime", "Time", "time"};
    std::vector<std::string> timeUniforms(timeUniformArgs, timeUniformArgs + 3);
    const char* mouseUniformArgs[] = {"iMouse", "Mouse", "mouse"};
    std::vector<std::string> mouseUniforms(mouseUniformArgs, mouseUniformArgs + 3);
    const char* resolutionUniformArgs[] = {"iResolution", "Resolution", "resolution"};
    std::vector<std::string> resolutionUniforms(resolutionUniformArgs, resolutionUniformArgs + 3);
    const char* textureUniformArgs[] = {"iTexture", "Texture", "texture", "iChannel"};
    std::vector<std::string> textureUniforms(textureUniformArgs, textureUniformArgs + 4);
    const char* resolutionSelfUniformArgs[] = {"iResolutionSelf", "ResolutionSelf", "resolutionself"};
    std::vector<std::string> resolutionSelfUniforms(resolutionSelfUniformArgs, resolutionSelfUniformArgs + 3);
    const char* feedbackTextureUniformArgs[] = {"iFeedback", "Feedback", "feedback"};
    std::vector<std::string> feedbackTextureUniforms(feedbackTextureUniformArgs, feedbackTextureUniformArgs + 3);

    GLsizei outputWidth = outputFrameBuffer->width();
    GLsizei outputHeight = outputFrameBuffer->height();
    float currentFrame = outputContext().frame();
    float currentTime = currentFrame / frameRate;
    //float mousePos[2] = { mouseposition[0], mouseposition[1] };

    // General input data uniforms
    GLint generalLocation;
    std::string uniformName;

    // Iteration uniforms, this feeds the iteration count to shader so we know what the max count is
    for (int i = 0; i < iterCountUniforms.size(); ++i)
    {
        uniformName = iterCountUniforms.at(i);
        generalLocation = glGetUniformLocation(programID, uniformName.c_str());
        glUniform1f(generalLocation, numIterations);
    }

    // Frame uniforms
    for (int i = 0; i < frameUniforms.size(); ++i)
    {
        uniformName = frameUniforms.at(i);
        generalLocation = glGetUniformLocation(programID, uniformName.c_str());
        glUniform1f(generalLocation, currentFrame);
    }

    // Time uniforms
    for (int i = 0; i < timeUniforms.size(); ++i)
    {
        uniformName = timeUniforms.at(i);
        generalLocation = glGetUniformLocation(programID, uniformName.c_str());
        glUniform1f(generalLocation, currentTime);
    }

    // Mouse uniforms
    for (int i = 0; i < mouseUniforms.size(); ++i)
    {
        uniformName = mouseUniforms.at(i);
        generalLocation = glGetUniformLocation(programID, uniformName.c_str());
        glUniform2fv(generalLocation, 1, mouseposition);
    }

    // Camera uniforms

    uniformName = "camFocalLength";
    generalLocation = glGetUniformLocation(programID, uniformName.c_str());
    glUniform1f(generalLocation, cameraData.focal_length);

    uniformName = "camPosition";
    generalLocation = glGetUniformLocation(programID, uniformName.c_str());
    glUniform3fv(generalLocation, 1, cameraData.position);

    uniformName = "camFilmBack";
    generalLocation = glGetUniformLocation(programID, uniformName.c_str());
    glUniform2fv(generalLocation, 1, cameraData.filmback);

    uniformName = "camNear";
    generalLocation = glGetUniformLocation(programID, uniformName.c_str());
    glUniform1f(generalLocation, cameraData.nearDistance);

    uniformName = "camFar";
    generalLocation = glGetUniformLocation(programID, uniformName.c_str());
    glUniform1f(generalLocation, cameraData.farDistance);

    uniformName = "camFocalPoint";
    generalLocation = glGetUniformLocation(programID, uniformName.c_str());
    glUniform1f(generalLocation, cameraData.focal_point);

    uniformName = "camFStop";
    generalLocation = glGetUniformLocation(programID, uniformName.c_str());
    glUniform1f(generalLocation, cameraData.fstop);

    uniformName = "camWinTranslate";
    generalLocation = glGetUniformLocation(programID, uniformName.c_str());
    glUniform2fv(generalLocation, 1, cameraData.win_translate);

    uniformName = "camWinScale";
    generalLocation = glGetUniformLocation(programID, uniformName.c_str());
    glUniform2fv(generalLocation, 1, cameraData.win_scale);

    uniformName = "camWinRoll";
    generalLocation = glGetUniformLocation(programID, uniformName.c_str());
    glUniform1f(generalLocation, cameraData.win_roll);

    uniformName = "camProjectionMode";
    generalLocation = glGetUniformLocation(programID, uniformName.c_str());
    glUniform1f(generalLocation, cameraData.proj_mode);

    uniformName = "camProjectionIsLinear";
    generalLocation = glGetUniformLocation(programID, uniformName.c_str());
    glUniform1f(generalLocation, cameraData.projection_is_linear);

    uniformName = "camProjection";
    generalLocation = glGetUniformLocation(programID, uniformName.c_str());
    glUniformMatrix4fv(generalLocation, 1, GL_FALSE, cameraData.projection);

    uniformName = "camMatrix";
    generalLocation = glGetUniformLocation(programID, uniformName.c_str());
    glUniformMatrix4fv(generalLocation, 1, GL_FALSE, cameraData.matrix);

    uniformName = "camMatrixInv";
    generalLocation = glGetUniformLocation(programID, uniformName.c_str());
    glUniformMatrix4fv(generalLocation, 1, GL_FALSE, cameraData.imatrix);



    // Texture uniforms
    GLenum texBaseAddress = GL_TEXTURE0;
    GLuint TextureLocations[MAX_INPUTS] = {0};
    GLint ResolutionLocations[MAX_INPUTS] = {0};
    for (int i = 0; i < inputFrameBuffers.size(); ++i)
    {
        FrameBuffer* fb = inputFrameBuffers.at(i);
        if (fb && fb->initialized())
        {
            // Bind texture uniforms to texture name
            for (int u = 0; u < textureUniforms.size(); ++u)
            {
                std::string textureName = textureUniforms.at(u) + tostr(i);
                TextureLocations[i] = glGetUniformLocation(programID, textureName.c_str());
                glActiveTexture(texBaseAddress + i);
                glBindTexture(GL_TEXTURE_2D, fb->texture());
                glUniform1i(TextureLocations[i], i);
                //std::cout << "Framebuffer " << i << " location in shader: " << TextureLocations[i] << std::endl;

                // A general case with no index from first input that some shaders use
                if (i == 0)
                {
                    textureName = textureUniforms.at(u);
                    TextureLocations[i] = glGetUniformLocation(programID, textureName.c_str());
                    glActiveTexture(texBaseAddress + i);
                    glBindTexture(GL_TEXTURE_2D, fb->texture());
                    glUniform1i(TextureLocations[i], i);
                }
            }

            // Bind texture resolutions to uniforms
#ifdef _WIN32
            float bufferResolution[2] = {fb->width(), fb->height()};
#else
            float bufferResolution[2] = {fb->width(), fb->height()};
#endif


            for (int u = 0; u < resolutionUniforms.size(); ++u)
            {
                std::string resolutionName = resolutionUniforms.at(u) + tostr(i);
                ResolutionLocations[i] = glGetUniformLocation(programID, resolutionName.c_str());
                glUniform2fv(ResolutionLocations[i], 1, bufferResolution);

                // A general case with no index from first input that some shaders use
                if (i == 0)
                {
                    resolutionName = resolutionUniforms.at(u);
                    ResolutionLocations[i] = glGetUniformLocation(programID, resolutionName.c_str());
                    glUniform2fv(ResolutionLocations[i], 1, bufferResolution);
                }
            }
        }
    }


    // Here we bind the output buffer to special input uniform called feedbackTexture
    // This mechanism makes the output accessible when doing multiple iterations
    // Texture uniforms
    GLenum feedbackTexBaseAddress = GL_TEXTURE8;
    GLuint feedbackTextureLocation = 0;
    GLint ResolutionLocation = 0;
    FrameBuffer* fb = intermediateFrameBuffer;
    if (fb)
    {
        for (int i = 0; i < feedbackTextureUniforms.size(); ++i)
        {
            uniformName = feedbackTextureUniforms.at(i);
            feedbackTextureLocation = glGetUniformLocation(programID, uniformName.c_str());
            glActiveTexture(feedbackTexBaseAddress);
            glBindTexture(GL_TEXTURE_2D, fb->texture());
            glUniform1i(feedbackTextureLocation, 8);
            //std::cout << "Feedback location in shader: " << feedbackTextureLocation << std::endl;
        }

        // Bind texture resolutions to uniforms
#ifdef _WIN32
        float bufferResolution[2] = { (float)fb->width(), (float)fb->height()} ;
#else
        float bufferResolution[2] = { (float)fb->width(), (float)fb->height() };
#endif

        for (int u = 0; u < resolutionSelfUniforms.size(); ++u)
        {
            std::string resolutionName = resolutionSelfUniforms.at(u) + tostr(u);
            ResolutionLocation = glGetUniformLocation(programID, resolutionName.c_str());
            glUniform2fv(ResolutionLocation, 1, bufferResolution);
        }
    }

    // 1rst attribute buffer : vertices
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glVertexAttribPointer(
        0, // The attribute we want to configure
        3,                  // size
        GL_FLOAT,           // type
        GL_FALSE,           // normalized?
        0,                  // stride
        (void*)0            // array buffer offset
    );


    // If necessary buffers have not been created by this stage, create them.
    // They should be but sometimes they are not.
    if (!oglBuffersCreated)
        createOGLBuffers();

    glBindFramebuffer(GL_FRAMEBUFFER, RenderFramebuffer);

    // Set the list of draw buffers for intermediate renders
    GLenum RenderDrawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
    glDrawBuffers(1, RenderDrawBuffers);

    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        logMessage("glCheckFramebufferStatus renderbuffer error");
        logMessage(std::to_string(glCheckFramebufferStatus(GL_FRAMEBUFFER)));
	    //glfwMakeContextCurrent(NULL);
        return false;
    }

    glBindFramebuffer(GL_FRAMEBUFFER, FinalFramebuffer);

    // Set the list of draw buffers for final drawing
    GLenum finalDrawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
    glDrawBuffers(1, finalDrawBuffers);

    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        logMessage("glCheckFramebufferStatus finalbuffer error");
        logMessage(std::to_string(glCheckFramebufferStatus(GL_FRAMEBUFFER)));
	    glfwMakeContextCurrent(NULL);
        return false;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Final buffer is now set as draw buffer and bound for drawing
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    glViewport(0, 0, outputFrameBuffer->width(), outputFrameBuffer->height()); //
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

    // If we do more than one pass:
    if (numIterations > 1)
    {
        // We need to set render result into intermediate only if we loop
        // Set renderbuffer as Read buffer, output buffer as Draw buffer and blit (copy) from renderbuffer to output
        glBindFramebuffer(GL_READ_FRAMEBUFFER, FinalFramebuffer);
        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, RenderFramebuffer);
        glBlitFramebuffer(0, 0, outputWidth, outputHeight, 0, 0, outputWidth, outputHeight, GL_COLOR_BUFFER_BIT, GL_LINEAR);

        for (int c = 0; c < (numIterations - 1); ++c)
        {
            // Currently we have rendered stuff in output buffer

            // Set output buffer texture as uniform for shader
            for (int i = 0; i < feedbackTextureUniforms.size(); ++i)
            {
                uniformName = feedbackTextureUniforms.at(i);
                feedbackTextureLocation = glGetUniformLocation(programID, uniformName.c_str());
                glActiveTexture(GL_TEXTURE0);
                glBindTexture(GL_TEXTURE_2D, finalFramebufferTexture);
                glUniform1i(feedbackTextureLocation, 0);
            }

            // Set current iteration uniform
            for (int i = 0; i < iterUniforms.size(); ++i)
            {
                uniformName = iterUniforms.at(i);
                generalLocation = glGetUniformLocation(programID, uniformName.c_str());
                glUniform1f(generalLocation, c + 1);
            }

            // Bind render buffer as draw buffer and draw using glViewport
            glBindFramebuffer(GL_DRAW_FRAMEBUFFER, RenderFramebuffer);

            glViewport(0, 0, outputFrameBuffer->width(), outputFrameBuffer->height()); //
            glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

            // Set renderbuffer as Read buffer, output buffer as Draw buffer and blit (copy) from renderbuffer to output
            glBindFramebuffer(GL_READ_FRAMEBUFFER, RenderFramebuffer);
            glBindFramebuffer(GL_DRAW_FRAMEBUFFER, FinalFramebuffer);
            glBlitFramebuffer(0, 0, outputWidth, outputHeight, 0, 0, outputWidth, outputHeight, GL_COLOR_BUFFER_BIT, GL_LINEAR);

            // Now we have new result in output buffer texture and we can loop again
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Copy from outputbuffer to output array and do Nuke stuff
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // Bind output buffer texture
    glBindTexture(GL_TEXTURE_2D, finalFramebufferTexture);

    // Copy final render texture to float buffer which is used as node output
    float* outputBuffer = outputFrameBuffer->packedDataPointer(outputFrameBuffer->size(), 4);
    glGetTexImage (GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, outputBuffer);

    glBindTexture(GL_TEXTURE_2D, 0);

    glDisableVertexAttribArray(0);

    // Swap buffers. Not sure this is necessary
    glfwSwapBuffers(window);
    glfwPollEvents();

    glfwMakeContextCurrent(NULL);

    return true;
}



void NukeGLSL::engine (int y, int x, int r, DD::Image::ChannelMask channels, DD::Image::Row& row)
{
    // /////////////////////////////////////////////
    // Actual workload
    // /////////////////////////////////////////////

    if (outputFrameBuffer)
    {
        int size = outputFrameBuffer->size() * 4;
        int width = outputFrameBuffer->width();
        float* dataPtr = outputFrameBuffer->packedData();

        // Copy data from imagebuffer
        foreach ( z, channels )
        {
            if (z > 0 && z < 5)
            {
                float* outptr = row.writable(z) + x;
                for( int cur = x ; cur < r; cur++ )
                {
                    int offset = (y * width + cur) * 4;
                    if (offset > size) offset = 0;
                    *outptr++ = dataPtr[offset + z - 1];
                }
            }
        }
    } else {
        // Normal interpolation from input data, set to gray for debugging
        foreach ( z, channels )
        {
            float* outptr = row.writable(z) + x;
            for( int cur = x ; cur < r; cur++ )
            {
                float value = 0.18;
                *outptr++ = value;
            }
        }
    }
}


static DD::Image::Iop* build(Node* node)
{
    return (new DD::Image::NukeWrapper(new NukeGLSL(node)))->channelsRGBoptionalAlpha();
}

const DD::Image::Op::Description NukeGLSL::description(CLASS, "Image/Filter/NukeGLSL", build);
